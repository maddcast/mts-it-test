package com.example.mtsittest.service.impl;

import com.example.mtsittest.entity.Task;
import com.example.mtsittest.repository.TaskRepository;
import com.example.mtsittest.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;


    @Override
    public void addTask(Task task) {
        taskRepository.saveAndFlush(task);
    }

    @Override
    public Task getTask(String guid) {
        return taskRepository.getOne(guid);
    }

    @Override
    public void editTask(Task task) {
        taskRepository.saveAndFlush(task);
    }

    @Override
    public void changeStatus(Task task, String status) {
        task.setStatus(status);
        task.setTs(new Date());
        editTask(task);
    }
}

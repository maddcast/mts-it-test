package com.example.mtsittest.service;

import com.example.mtsittest.entity.Task;

public interface TaskService {
    void addTask(Task task);
    Task getTask(String guid);
    void editTask(Task task);
    void changeStatus(Task task, String status);
}

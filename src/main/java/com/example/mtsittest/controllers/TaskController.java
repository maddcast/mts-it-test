package com.example.mtsittest.controllers;

import com.example.mtsittest.entity.Task;
import com.example.mtsittest.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;


    @RequestMapping("/task")
    public void createTask(HttpServletResponse response) {

        Task task = new Task();
        task.setGuid(UUID.randomUUID().toString());
        task.setStatus("created");
        task.setTs(new Date());

        taskService.addTask(task);

        setResponse(response, 202, "{ \"guid\": \"" + task.getGuid() + "\" }");

        taskService.changeStatus(task, "running");

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                taskService.changeStatus(task, "finished");
                timer.purge();
            }
        }, 1000 * 60 * 2);
    }


    @RequestMapping("/task/{id}")
    public void getTask(@PathVariable String id,
                        HttpServletResponse response) {
        Pattern pattern = Pattern.compile("\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}");
        Matcher matcher = pattern.matcher(id);
        if (!matcher.matches()) {
            setResponse(response, 400, "Invalid parameter: ID");
            return;
        }

        Task task = taskService.getTask(id);

        if (task == null) {
            setResponse(response, 404, "Not found");
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        setResponse(response, 200, "{ \"status\": \"" + task.getStatus() + "\", \"timestamp\": \"" + sdf.format(task.getTs()) + "\" }");
    }


    private void setResponse(HttpServletResponse response, int status, String text) {
        try {
            response.setStatus(status);
            response.getWriter().print(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

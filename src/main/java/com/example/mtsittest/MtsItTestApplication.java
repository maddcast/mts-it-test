package com.example.mtsittest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class MtsItTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MtsItTestApplication.class, args);
	}

}
